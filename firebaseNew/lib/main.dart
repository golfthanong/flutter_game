import 'package:flutter/material.dart';
import 'package:firebasenew/screen/fromscreen.dart';
import 'package:firebasenew/screen/display.dart';
import 'package:firebasenew/screen/gamescreen.dart';
import 'package:firebase_core/firebase_core.dart';

void main()async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;



  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: TabBarView(
          children: [

            Gamesrceen(),
            Fromscreen(),
            DisplayScreen()//from กรอกประวัตินักเรียน อยู่ใน fromscreen.dart เป็น Container
          ],
        ),

        backgroundColor: Color(0xffedce71),
        bottomNavigationBar: TabBar(
          tabs: [
            Tab(text: "หน้าเกมส์",),
            Tab(text: "บันทึกคะแนน",),
            Tab(text: "รายชื่อนักเรียน",)



          ],
        ), // TabBar
      ), // Scaffold
    );
   // return  Gamesrceen();
  }
}
