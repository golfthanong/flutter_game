import 'package:flutter/material.dart';
import 'package:firebasenew/model/block_unit.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebasenew/functions/buildMenu.dart';
import 'package:firebasenew/model/block_unit_manager.dart';
import 'package:firebasenew/functions/buildVocabulary.dart';
import 'package:firebasenew/model/block_char.dart';
import 'package:firebasenew/model/word.dart';

class Gamesrceen extends StatefulWidget {
  const Gamesrceen({Key? key}) : super(key: key);

  @override
  State<Gamesrceen> createState() => _GamesrceenState();
}

const double BLOCK_SIZE_WIDTH = 45;
const double BLOCK_SIZE_HEIGHT = 70;
const int DIRECTION_UP = 0;
const int DIRECTION_LEFT = 1;
const int DIRECTION_RIGHT = 2;
const int DIRECTION_DOWN = 3;

class _GamesrceenState extends State<Gamesrceen> {
  late List<List<BlockUnit>> table;

  late List<List<BlockChar>> tableChar;

  bool delayMode = false;
  int score = 0;

  @override
  void initState() {
    initTable();
    super.initState();
  }

  void initTable() {
    tableChar = [];
    for (int row = 0; row < 8; row++) {
      List<BlockChar> listChar = [];
      for (int col = 0; col < 8; col++) {
        listChar.add(BlockChar('A', Color(0xffeee4d9), Color(0xff776e64), 32,
            'https://picsum.photos/700/600'));
      }

      tableChar.add(listChar);
    }
  }

  @override
  Widget build(BuildContext context) {
    int _counter = 0;

    void _incrementCounter() {
      setState(() {
        _counter++;
      });
    }

    return Scaffold(
        //appBar: AppBar(title: Text("Game Ver"),backgroundColor: Color(0xffbbada0),),

        floatingActionButton: FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: const Icon(Icons.done),
        ),
        body: Container(
            color: Color(0xffedce71),

            //child: buildMenu(0);

            child: Column(children: <Widget>[
              buildMenu(0),
              //buildControlButton(),

              Expanded(
                child: StreamBuilder(
                    stream: FirebaseFirestore.instance
                        .collection("students")
                        .snapshots(),
                    builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (!snapshot.hasData) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return Container(
                            color: Color(0xfffbf9f3),
                            child: Center(
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Color(0xffbaad9e),
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(
                                          width: 6, color: Color(0xffbaad9e))),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Image.network(
                                        'https://picsum.photos/700/600',
                                        width: 400,
                                        height: 400,
                                        fit: BoxFit.cover,
                                      ),
                                    ],
                                  )),
                            ));
                      }
                    }),
              ),

              StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("1")
                    .snapshots(),
                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  List<Word>? word = [];

                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {


                   return Row(
                      children: snapshot.data!.docs.map((collection){

                        word.add(Word(collection["first"].toString(),collection["second"].toString()));
print(collection["first"].toString());
                        print(word[0].value.toString());

                       // return buildBlockUnitChar(1, 1,word[0].value.toString());
                        return buildControlButtonChar();
                      }
                      ).toList(),

                    );

                  }
                },
              ),
            ])));
  }

  void OnCounterButtonPressed() {
    setState(() {
      //_counter = _counter+1;
    });
  }

//แบบตัวอักษร
  Container buildControlButtonChar() {
    return Container(
        decoration: BoxDecoration(
            color: Color(0xffbaad9e),
            borderRadius: BorderRadius.circular(8),
            border: Border.all(width: 6, color: Color(0xffbaad9e))),
        child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: buildTableChar()));
  }

  List<Row> buildTableChar() {
    //จำนวนแถวที่ส่งไป
    List<Row> listRow = [];

    listRow.add(Row(
        mainAxisSize: MainAxisSize.min, children: buildRowBlockUnitChar(0)));

    return listRow;
  }

  List<Widget> buildRowBlockUnitChar(int row) {
    List<Widget> list = [];
    for (int col = 0; col < 6; col++) {
      list.add(buildBlockUnitChar(row, 0,"A"));
    }
   return list;
  }

  Container buildBlockUnitChar(int row, int col,String char) {
    //สร้างช่องในเกมส์

    tableChar[row][col].value = char;
    return Container(
      decoration: BoxDecoration(
        color: tableChar[row][col].colorBackground,
        borderRadius: BorderRadius.circular(4), //ความโค้งของช่อง
      ),
      width: BLOCK_SIZE_WIDTH,
      height: BLOCK_SIZE_HEIGHT,
      margin: EdgeInsets.all(3),
      //ความห่างของช่อง
      child: Center(
          child: Text(
        "" + tableChar[row][col].value.toString(),
        style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.bold,
            color: tableChar[row][col].colorText),
      )),
    );
  }
}
