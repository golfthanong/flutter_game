import 'package:flutter/material.dart';
import 'package:firebasenew/screen/gamescreen.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      title: Text('golf'),
    ),
      body: SingleChildScrollView( //ถ้าหน้าจอใหญ่เกินจะมีสกอร์
        child: Column(
          children: [
            SizedBox(
              width: double.infinity, // ให้ปุ่มยาวเท่ากับหน้าจอ
              child: ElevatedButton.icon(
                onPressed: () {//พอกดปุ่มให้ไปที่หน้า Gamesrceen
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return Gamesrceen();
                  }));
                },
                icon: Icon(Icons.add),
                  label: Text("สร้างข้อมูล", style:TextStyle(fontSize: 20)),
              ),
            )
        ],
    ),
      ),
    );
  }
}
