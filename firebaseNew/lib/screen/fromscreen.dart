import 'package:flutter/material.dart';
import 'package:firebasenew/model/student.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Fromscreen extends StatefulWidget {
  const Fromscreen({Key? key}) : super(key: key);

  @override
  State<Fromscreen> createState() => _FromscreenState();
}

class _FromscreenState extends State<Fromscreen> {
  final formkey = GlobalKey<FormState>();
  Student mystudent = Student("1", "1", "1", "1");

  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  CollectionReference studentCollection =
      FirebaseFirestore.instance.collection("students");

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
      if (snapshot.hasError) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Error"),
          ),
          body: Center(
            child: Text("${snapshot.error}"),
          ),
        ); // Scaffold
      }
      if (snapshot.connectionState == ConnectionState.done) {
        return Scaffold(
            appBar: AppBar(
              title: Text("แบบฟอร์มบันทึกคะแนนสอบ"),
            ),
            body: Container(
              padding: EdgeInsets.all(20), //ระยะห่างระหว่างwid ในแต่ละอัน

              child: Form(
                  key: formkey,
                  child: SingleChildScrollView(
                    //ทำให้มี Scroll สามารถเลื่อนได้
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                        //ทำให้ object ชิดซ้าย
                        children: [
                          Text(
                            "ชื่อนักเรียน",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          TextFormField(
                            validator:
                                RequiredValidator(errorText: "กรุณากรอกข้อมูล"),
                            //การ validate ค่าใน field
                            onSaved: (value) {
                              mystudent.fname = value;
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "นามสกุลนักเรียน",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          TextFormField(
                            onSaved: (value) {
                              mystudent.lname = value.toString();
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Email",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          TextFormField(
                            validator: MultiValidator([
                              //ใช้ตรวจสอบค่าที่กรอกใน field มากกว่า 1 ค่า เช่นรูปแบบของ Emailและค่าว่าง
                              EmailValidator(
                                  errorText: "กรุณากรอก Email ให้ถูกต้อง"),
                              RequiredValidator(errorText: "กรุณากรอก Email "),
                            ]),
                            onSaved: (value) {
                              mystudent.email = value.toString();
                            },
                            keyboardType: TextInputType
                                .emailAddress, //ทำให้ keyboradที่แสดงขึ้นมาเป็นแบบกรอก emailได้
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "คะแนน",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          TextFormField(
                            //validator:TextInputType.number ,
                            onSaved: (value) {
                              mystudent.score = value.toString();
                            },
                            keyboardType: TextInputType.number,
                          ),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              child: Text(
                                " บันทึกข้อมูล",
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                              onPressed: () async{
                                if (formkey.currentState!.validate()) {
                                  //ตรวจสอบใน fromkey  ว่า validate เป็นค่า true หรือยัง
                                  formkey.currentState
                                      ?.save(); // เปลี่ยน status เป็น onSave ทำงานฟังก์ชัน onSaveทำงาน
                                 await studentCollection.add({
                                      "fname":mystudent.fname,
                                      "1name":mystudent.lname,
                                      "email": mystudent.email,
                                      "score":mystudent.score
                                  });
                                  formkey.currentState?.reset();
                                  //print("ชื่อ${mystudent.fname}");
                                  //print("นาม${mystudent.lname}");
                                  //print("mail${mystudent.email}");
                                  //print("คะแนน${mystudent.score}");
                                }
                              },
                            ),
                          ),
                        ]),
                  )), // Form
            ));
      }
      //if(snapshot.connectionState ConnectionState.done){}

      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      ); // Scaffold
    });

  }
}
