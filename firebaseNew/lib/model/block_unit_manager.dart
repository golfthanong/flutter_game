import 'block_unit.dart';
import 'dart:math';
import 'package:flutter/material.dart';

const int COUNT_BLOCK_TYPE = 12;
const int BLOCK_VALUE_NONE = 1;
const int BLOCK_VALUE_2 = 2;
const int BLOCK_VALUE_4 = 4;
const int BLOCK_VALUE_8 = 8;
const int BLOCK_VALUE_16 = 16;
const int BLOCK_VALUE_32 = 32;
const int BLOCK_VALUE_64 = 64;
const int BLOCK_VALUE_128 = 128;
const int BLOCK_VALUE_256 = 256;
const int BLOCK_VALUE_512 = 512;
const int BLOCK_VALUE_1024 = 1024;
const int BLOCK_VALUE_2048 = 2048;
const int BLOCK_VALUE_4096 = 4096;
const int BLOCK_VALUE_8192 = 8192;
const int BLOCK_VALUE_16384 = 16384;
const int BLOCK_VALUE_32768 = 32768;

class BlockUnitManager {
  static BlockUnit randomBlock({int maxPow = COUNT_BLOCK_TYPE}) {
    Random random = Random();
    int value = pow(2, random.nextInt(6)).toInt();
    return create(value);
  }

  static BlockUnit randomSimpleBlock() {
    Random random = Random();
    int value = random.nextInt(2);
    if (value == 0) {
      return create(BLOCK_VALUE_2);
    }
    return create(BLOCK_VALUE_4);
  }

  static BlockUnit createVoca(int value) {
    return BlockUnit(value, Color(0xffccc0b3), Color(0x00ffffff), 32);

  }

  static BlockUnit create(int value) {
    if (value == BLOCK_VALUE_NONE) {
      return BlockUnit(value, Color(0xffccc0b3), Color(0x00ffffff), 32);
    } else if (value == BLOCK_VALUE_2) {
      return BlockUnit(value, Color(0xffeee4d9), Color(0xff776e64), 32);
    } else if (value == BLOCK_VALUE_4) {
      return BlockUnit(value, Color(0xffede0c8), Color(0xff776e64), 32);
    }else if (value == BLOCK_VALUE_8) {
      return BlockUnit(value, Color(0xfff2b179), Color(0xffffffff), 32);
    }else if (value == BLOCK_VALUE_16) {
      return BlockUnit(value, Color(0xfff49663), Color(0xffffffff), 32);
    }else if (value == BLOCK_VALUE_32) {
      return BlockUnit(value, Color(0xfff77b63), Color(0xffffffff), 32);
    }else if (value == BLOCK_VALUE_64) {
      return BlockUnit(value, Color(0xfff45639), Color(0xffffffff), 32);
    }else if (value == BLOCK_VALUE_128) {
      return BlockUnit(value, Color(0xffedce71), Color(0xffffffff), 32);
    }else if (value == BLOCK_VALUE_256) {
      return BlockUnit(value, Color(0xfff0cb63), Color(0xffffffff), 32);
    }else  {
      return BlockUnit(value, Color(0xff8e44ad), Color(0xffffffff), 32);
    }
  }
}