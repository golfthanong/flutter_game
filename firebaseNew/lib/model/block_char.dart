import 'package:flutter/material.dart';
class BlockChar {
  String value;
  Color colorBackground;
  Color colorText;
  double fontSize;
  String imageLink;

  BlockChar(this.value, this.colorBackground, this.colorText, this.fontSize,this.imageLink);
}