import 'package:flutter/material.dart';
class Coordinate {
  int row;
  int col;

  Coordinate(this.row, this.col);
}