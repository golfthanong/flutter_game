import 'package:flutter/material.dart';

class BlockUnit {
  int value;
  Color colorBackground;
  Color colorText;
  double fontSize;

  BlockUnit(this.value, this.colorBackground, this.colorText, this.fontSize);
}

