import 'package:flutter/material.dart';
import 'package:firebasenew/model/block_unit.dart';
late List<List<BlockUnit>> table =[];
const double BLOCK_SIZE = 45;
Container buildVocabulary(int score) {
  return Container(
    padding: EdgeInsets.only(top: 30, bottom: 15, left: 16, right: 16),
    //กรอบของปุ่มเกมส์ ว่าจะเหลือขอบด้านไหนเท่าไหร่
    color: Color(0xffede0c8),
    child: Row(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
      GestureDetector(
          onTap: () {
            //
          },
          child: Container(
              constraints: BoxConstraints(minWidth: 350,minHeight: 100),//ขนาดของปุ่ม New Game
              decoration: BoxDecoration( //por การตบแต่งต้องส่งค่าเป็น BoxDecoration
                  color: Color(0xff8f7a66),
                  borderRadius: BorderRadius.circular(4)),
              padding: EdgeInsets.all(10),
              child: Row(children:
              <Widget>[
                buildBlockUnit(0,0),

              ]
              )
          )
      ),

    ]),
  );


  }

Container buildBlockUnit(int row, int col) {
  //สร้างช่องในเกมส์
  return Container(
    decoration: BoxDecoration(
      color: table[row][col].colorBackground,
      borderRadius: BorderRadius.circular(4), //ความโค้งของช่อง
    ),
    width: BLOCK_SIZE,
    height: BLOCK_SIZE,
    margin: EdgeInsets.all(3),
    //ความห่างของช่อง
    child: Center(
        child: Text(
          "" + table[row][col].value.toString(),
          style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
              color: table[row][col].colorText),
        )),
  );
}


