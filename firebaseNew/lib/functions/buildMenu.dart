import 'package:flutter/material.dart';
Container buildMenu(int score) {
  return Container(
    padding: EdgeInsets.only(top: 65, bottom: 12, left: 16, right: 16),
    color: Color(0xffede0c8),
    child:
    Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      GestureDetector(
          onTap: () {
            //
          },
          child: Container(
              constraints: BoxConstraints(minWidth: 100),
              decoration: BoxDecoration(
                  color: Color(0xfff2b179),
                  borderRadius: BorderRadius.circular(4)),
              padding: EdgeInsets.all(12),
              child: Column(children: <Widget>[
                Text("New Game",
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white))
              ]))),
      Expanded(child: Container()),
      Container(
          constraints: BoxConstraints(minWidth: 120),
          decoration: BoxDecoration(
              color: Color(0xffbbada0),
              borderRadius: BorderRadius.circular(4)),
          padding: EdgeInsets.all(4),
          child: Column(children: <Widget>[
            Text("SCORE",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),

          ]))
    ]),
  );
}